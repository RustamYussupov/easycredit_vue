module.exports = {
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@/assets/scss/base/variables.scss";`
            }
        }
    }
};