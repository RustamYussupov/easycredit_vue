import axios from 'axios';

export default {
    namespaced: true,
    state: {
        discounts: {
            'default': {
                'title': 'default',
                'text': 'Весь март на первый займ от default.kz',
                'condition': 'До 75 000 тенге',
                'fullText': '0% весь март на первый займ от default.kz до 75 000 тг',
                'date': '13 января',
                'link': '#',
                'is_percent': false,
                'styles': {
                    'bg': '#80D82E',
                    'bg_title': '#E91E63',
                    'color_title': '#FFFFFF',
                    'color_text': '#FFFFFF'
                }
            }
        }
    },
    getters: {
        DISCOUNTS (state) {
            return state.discounts;
        }
    },
    mutations: {
        SET_DISCOUNTS_TO_STATE (state, response) {
            state.discounts = response;
        }
    },
    actions: {
        async GET_DISCOUNTS_FROM_API ({commit}) {
            await axios
                .get('http://localhost:3000/discounts')
                .then((response) => {
                    commit('SET_DISCOUNTS_TO_STATE', response.data);
                })
                .catch(error => console.log(error));
        }
    }
};