import axios from 'axios';

export default {
    namespaced: true,
    state: {
        terms: {}
    },
    getters: {
        TERMS (state) {
            return state.terms;
        },
        TERMS_ON_MAIN (state) {
            let termsOnMain = {};

            for (let key in state.terms) {
                if (state.terms[key].inMain) {
                    termsOnMain[key] = state.terms[key];
                }
            }

            return termsOnMain;
        }
    },
    mutations: {
        UPDATE_CHECKED_TERMS (state, data) {
            state.terms[data.key].checked = data.flag;
        },
        SET_TERMS_TO_STATE (state, response) {
            state.terms = response;
        }
    },
    actions: {
        async GET_TERMS_FROM_API ({commit}) {
            await axios
                .get('http://localhost:3000/terms')
                .then((response) => {
                    commit('SET_TERMS_TO_STATE', response.data);
                })
                .catch(error => console.log(error));
        }
    }
};