import axios from 'axios';

export default {
    namespaced: true,
    state: {
        publications: {}
    },
    getters: {
        PUBLICATIONS (state) {
            return state.publications;
        }
    },
    mutations: {
        SET_PUBLICATION_TO_STATE (state, response) {
            state.publications = response;
        }
    },
    actions: {
        async GET_PUBLICATIONS_FROM_API ({commit}) {
            await axios
                .get('http://localhost:3000/publications')
                .then((response) => {
                    commit('SET_PUBLICATION_TO_STATE', response.data);
                })
                .catch(error => console.log(error));
        }
    }
};