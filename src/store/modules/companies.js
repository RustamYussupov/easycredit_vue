import axios from 'axios';

export default {
    namespaced: true,
    state: {
        companies: [],
        companiesToCatalog: []
    },
    getters: {
        COMPANIES(state) {
            return state.companies;
        },
        COMPANIES_TO_CATALOG(state) {
            return state.companiesToCatalog;
        },
    },
    mutations: {
        SET_COMPANIES_TO_STATE(state, response) {
            state.companies = response;
            state.companiesToCatalog = response;
        },
        TOGGLE_DESCRIPTION(state, index) {
            state.companies[index].toggle_description = !state.companies[index].toggle_description;
        },
        COMPANY_FILTER(state, data) {
            state.companiesToCatalog = state.companies.filter((elem) => {
                let validValuesOfCalculators = elem.max_sum >= data.sum && elem.max_days >= data.days,
                    validConditions = 0;

                if (data.terms.length) {
                    for (let conditionsItem of elem.conditions) {
                        for (let termsItem of data.terms) {
                            conditionsItem === termsItem.name ? validConditions++ : false;
                        }
                    }
                } else {
                    validConditions = 1;
                }

                return validValuesOfCalculators && validConditions;
            });
        }
    },
    actions: {
        async GET_COMPANIES_FROM_API({commit}) {
            await axios
                .get('http://localhost:3000/companies')
                .then((response) => {
                    commit('SET_COMPANIES_TO_STATE', response.data);
                })
                .catch(error => console.log(error));
        }
    }
};