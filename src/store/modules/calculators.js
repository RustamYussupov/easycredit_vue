import axios from 'axios';

export default {
    namespaced: true,
    state: {
        calculators: {},
        response: false
    },
    getters: {
        CALCULATORS(state) {
            return state.calculators;
        },
        RESPONSE(state) {
            return state.response;
        }
    },
    mutations: {
        SET_CALCULATORS_TO_STATE(state, response) {
            state.calculators = response.data;
        },
        SET_RESPONSE_TO_STATE(state, response) {
            state.response = response.statusText === "OK";
        },
        CHANGE_CALCULATOR_VALUE(state, data) {
            state.calculators[data.name].value = data.value;
        }
    },
    actions: {
        async GET_CALCULATORS_FROM_API({commit}) {
            await axios
                .get('http://localhost:3000/calculators')
                .then(response => {
                    commit('SET_CALCULATORS_TO_STATE', response);
                    commit('SET_RESPONSE_TO_STATE', response);
                })
                .catch(error => console.log(error));
        }
    }
};