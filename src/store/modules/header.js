export default {
    namespaced: true,
    state: {
        discountBlockFlag: false
    },
    getters: {
        DISCOUNT_BLOCK_FLAG(state) {
            return state.discountBlockFlag;
        }
    },
    mutations: {
        CLOSE_DISCOUNT_BLOCK(state) {
            state.discountBlockFlag = false;
        },
        OPEN_DISCOUNT_BLOCK(state) {
            state.discountBlockFlag = true;
        }
    },
    actions: {

    }
}
