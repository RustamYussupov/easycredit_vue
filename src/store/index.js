import Vue  from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import publications from './modules/publications';
import calculators  from './modules/calculators';
import terms        from './modules/terms';
import header       from './modules/header';
import discounts    from './modules/discounts';
import companies    from './modules/companies';

export default new Vuex.Store({
    modules: {
        publications,
        calculators,
        terms,
        header,
        discounts,
        companies,
    },
    strict: true
});
